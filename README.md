Minimal demo of a Qt issue
==========================

For investigation of a possible problem with static builds (at least on iOS) using Glow (and presumably other QtGraphicalEffects).

<https://bugreports.qt.io/browse/QTBUG-57205> probably most relevant issue in Qt Jira.

Status/Resolution
-----------------

**Fixed!**

* <https://bugreports.qt.io/browse/QTBUG-57205> closes with fix to Qt 5.9.2.
* A workround for earlier versions is now applied in this demonstrator's `MAKE-ios` script (go back to the `QTBUG-57205` tag on this repo for original bug submission).

Build
-----

NB First you will need to change, in whichever build script you use:

* The path to your Qt dir (and possibly version) in those scripts.  
* For `./MAKE-ios`, the `teamsetting.value` to your Apple Developer ID (or do I mean team ID?)
* For `./MAKE-android`, the paths set for `ANDROID_NDK_ROOT` and `ANDROID_SDK_ROOT` will probably need changing.

Build with:

* **OSX**: `./MAKE-mac` (builds `qtissue.app`)
* **iOS**: `./MAKE-ios` (builds `qtissue.xcodeproj` which should then be built with and deployed from xcode; xcode8 assumed).
* **Linux**: `./MAKE-linux` builds `./qtissue` (reference Debian 8.9 (Jessie); NB *not* expected to work with Debian's Qt4!)
* **Android**: `./MAKE-android` builds `deploy/bin/QtApp-debug.apk` which can be deployed to a device using adb install.
    * Note that `main.cpp` contains some android-specific code to redirect Qt logging (whether from use of things like `qInfo() << ` in C++ or `console.log` in QML) to android's logging where it can be observed with `adb logcat` (probably also with `| grep qtissue` to limit the spew).  However currently the app code doesn't actually include anything which would produce any such messages.

Use `./MAKE-clean` to cleanup when switching between target architectures to nuke any build-product detritus.

When running, should display a green Qt logo surrounded by a pulsating yellow glow effect, on a black background with a yellow text caption at the bottom of the screen.

Problem
-------

On iOS, logs:
```
...
JIT is disabled for QML. Property bindings and animations will be very slow. Visit https://wiki.qt.io/V4 to learn about possible solutions for your platform.
qrc:///main.qml:27:3: Type Glow unavailable
qrc:///qt-project.org/imports/QtGraphicalEffects/Glow.qml:84:5: Type DropShadowBase unavailable
qrc:///qt-project.org/imports/QtGraphicalEffects/private/DropShadowBase.qml:58:5: Type GaussianBlur unavailable
qrc:///qt-project.org/imports/QtGraphicalEffects/GaussianBlur.qml:43:1: module "QtQuick.Window" plugin "windowplugin" not found
```
and the display remains blank white.

Possibly interestingly: if the `Glow` element is commented out in `main.qml`, the the app will work better (display the text), even with the `import QtGraphicalEffects 1.0` line retained.  Presumably implies the app does in some sense *have* QtGraphicalEffects available, but something fails when it tries to use something from it.

Notes
-----

* Besides the build scripts and `.pro` file and docs, there's not really anything more to the app than what's in `main.cpp` and `main.qml`.  The `qtissue.qrc` just ensures the qml and svg content are baked into the app.
* At time of writing, build scripts set to use Qt 5.8.
    * Qt 5.7 believed to also exhibit the issue.
    * Can't try Qt 5.9.1 until <https://bugreports.qt.io/browse/QTBUG-61690> is fixed; don't understand how the workround mentioned relates to the scripted build here.
    * Pretty sure Glow did work on iOS in Qt 5.6 because I remember needing <https://bugreports.qt.io/browse/QTBUG-47448> to be fixed.
* Whether `CONFIG += qtquickcompile` is used or not makes no difference (not that I'd have expected it to).
