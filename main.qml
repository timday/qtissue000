import QtQuick 2.5
import QtGraphicalEffects 1.0

Rectangle {
  id: main
  color: '#000000'

  Item {
    id: paddedlogo
    visible: false
    anchors.top: main.top
    anchors.left: main.left
    anchors.right: main.right
    anchors.bottom: caption.top
    anchors.margins: 0.25*Math.min(main.width,main.height)

    readonly property int padding: 32

    Image {
      id: logo
      anchors.centerIn: parent
      width: paddedlogo.width-2*paddedlogo.padding
      height: paddedlogo.height-2*paddedlogo.padding
      source: 'qrc:///Qt_logo.svg'  // From: https://en.wikipedia.org/wiki/File:Qt_logo_2016.svg
      fillMode: Image.PreserveAspectFit
      sourceSize.width: width
      sourceSize.height: height
      smooth: true
    }
  }

  Glow {
    id: glow
    anchors.fill: paddedlogo

    radius: paddedlogo.padding
    fast: true
    samples: 32
    color: '#ffff00'
    source: paddedlogo

    SequentialAnimation on spread {
      loops: Animation.Infinite
      NumberAnimation {duration: 500;easing.type: Easing.InOutSine;from:0.0;to:1.0}
      NumberAnimation {duration: 500;easing.type: Easing.InOutSine;from:1.0;to:0.0}
    }
  }

  Text {
    id: caption
    anchors.left: main.left
    anchors.right: main.right
    anchors.bottom: main.bottom
    anchors.margins: font.pixelSize
    text: '<b>QML</b>: <i>"Wow, it\'s HTML5 done right."</i> - node.js project founder'
    textFormat: Text.RichText
    font.pixelSize: main.width/32
    color: '#ffff00'
    horizontalAlignment: Text.AlignHCenter
    verticalAlignment: Text.AlignVCenter  
  }
}
