TEMPLATE = app

CONFIG += release
CONFIG += qtquickcompile

QT += core gui qml quick svg

TARGET = qtissue

SOURCES += main.cpp
RESOURCES += qtissue.qrc

android {
  DEFINES += ANDROIDQUIRKS
  ANDROID_PACKAGE_SOURCE_DIR=android/
}

ios {
  teamsetting.name = DEVELOPMENT_TEAM
  teamsetting.value = L3DR9E2979
  QMAKE_MAC_XCODE_SETTINGS += teamsetting

  idsetting.name = CODE_SIGN_IDENTITY
  idsetting.value = "iPhone Developer"
  QMAKE_MAC_XCODE_SETTINGS += idsetting
}

cache()
